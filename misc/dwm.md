## Fedora 31
```
yum install dwm-user
cp -v /usr/src/dwm-user*/config.def.h ~/.dwm/config.h
```

patch /usr/bin/dwm-start by adding:
```
[ -e ~/bin/dwm.sh ] && . ~/bin/dwm.sh
```

## config.h
```
#define MODKEY Mod4Mask
static const char *termcmd[]  = { "lxterminal", NULL };
static const char *tags[] = { "1", "2", "3", "4", "5", "6", "IM", "Mail", "Web" };
```

## Single-desktop dwm.sh
```
statusbar()
{
	while true; do
		xsetroot -name "`date +%H:%M`"
		sleep 60
	done
}

statusbar &

xsetroot -solid black
xgamma -bgamma 0.4
exec ~/.dwmbuild/dwm
```
